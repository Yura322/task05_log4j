package com.epam.sms;

import org.apache.logging.log4j.*;


public class Main {

    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        log.trace("trace");
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.error("error");
        log.fatal("fatal");
    }
}
